﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using NeuNet.Core;

namespace NeuNet
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Выделение буквы
        /// </summary>
        private bool IsSelecting = false;

        /// <summary>
        /// Область выделенной буквы
        /// </summary>
        private int X0, Y0, X1, Y1;

        Bitmap image;
        Bitmap grayImage;
        Bitmap shape;
        Bitmap matix;

        private int[] emptyMatrix = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        public int[] inputShape { get; set; }
        /// <summary>
        /// Образ Р
        /// </summary>
        public int[] shapeP { get; set; }
        /// <summary>
        /// Образ М
        /// </summary>
        public int[] shapeM { get; set; }

        /// <summary>
        /// Образ Е
        /// </summary>
        public int[] shapeE { get; set; }
        /// <summary>
        /// Образ В
        /// </summary>
        public int[] shapeB { get; set; }

        public Form1()
        {
            InitializeComponent();
            inputShape = emptyMatrix;
            shapeP = emptyMatrix;
            shapeM = emptyMatrix;
            shapeE = emptyMatrix;
            shapeB = emptyMatrix;
        }

         private void imageProcessing(Bitmap image)
        {
            image = ImagesFunctions.resizeBitmap(image, Properties.Settings.Default.width, Properties.Settings.Default.height);

            grayImage = ImagesFunctions.toGrayScale(new Bitmap(image));

            var scannedText = new Bitmap(grayImage);

            int x, y;

            for (x = 0; x < grayImage.Width; x++)
            {
                for (y = 0; y < grayImage.Height; y++)
                {
                    Color pixel = grayImage.GetPixel(x, y);

                    if (pixel.R < Properties.Settings.Default.ident && pixel.G < Properties.Settings.Default.ident && pixel.B < Properties.Settings.Default.ident)
                    {
                        scannedText.SetPixel(x, y, Color.Black);
                    }
                    else
                    {
                        scannedText.SetPixel(x, y, Color.White);
                    }
                }
            }

            groupBox2.Enabled = true;
            label1.Show();
            label2.Show();
            label3.Show();
            label4.Show();
            label12.Show();

            groupBox3.Enabled = true;

            shape = ImagesFunctions.resizeBitmap(ImagesFunctions.Crop(scannedText), Properties.Settings.Default.width, Properties.Settings.Default.height);

            button6.Enabled = true;

            matix = new Bitmap(shape);
            Pen pen = new Pen(Color.Red, 1);
            using (var graphics = Graphics.FromImage(matix))
            {
                for (int i = 10; i < 70; i = i + 10)
                {
                    graphics.DrawLine(pen, i, 0, i, Properties.Settings.Default.height);
                    graphics.DrawLine(pen, 0, i, Properties.Settings.Default.width, i);
                }
            }

            pictureBox1.Image = image;
            pictureBox2.Image = grayImage;
            pictureBox3.Image = shape;
            pictureBox4.Image = matix;
        }

        /// <summary>
        /// кнопка выбора изображения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string filePath = FileSystem.choose_file();

            try
            {
                image = new Bitmap(filePath, true);
                image = ImagesFunctions.resizeBitmap(image, 588, 307);
                groupBox4.Enabled = true;
                pictureBox5.Image = image;
                label5.Show();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Ошибка. Проверьте путь к файлу изображения.");
            }
        }

        /// <summary>
        /// Начало выбра области
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox5_MouseDown(object sender, MouseEventArgs e)
        {
            IsSelecting = true;

            // Save the start point.
            X0 = e.X;
            Y0 = e.Y;
        }

        private void pictureBox5_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsSelecting) return;

            X1 = e.X;
            Y1 = e.Y;

            Bitmap bm = new Bitmap(image);

            using (Graphics gr = Graphics.FromImage(bm))
            {
                gr.DrawRectangle(Pens.Red,
                    Math.Min(X0, X1), Math.Min(Y0, Y1),
                    Math.Abs(X0 - X1), Math.Abs(Y0 - Y1));
            }

            pictureBox5.Image = bm;
        }

        /// <summary>
        /// Конец выбора области
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox5_MouseUp(object sender, MouseEventArgs e)
        {
            if (!IsSelecting) return;
            IsSelecting = false;

            pictureBox5.Image = image;

            int wid = Math.Abs(X0 - X1);
            int hgt = Math.Abs(Y0 - Y1);
            if ((wid < 1) || (hgt < 1)) return;

            Bitmap area = new Bitmap(wid, hgt);
            using (Graphics gr = Graphics.FromImage(area))
            {
                Rectangle source_rectangle =
                    new Rectangle(Math.Min(X0, X1), Math.Min(Y0, Y1),
                        wid, hgt);
                Rectangle dest_rectangle =
                    new Rectangle(0, 0, wid, hgt);
                gr.DrawImage(image, dest_rectangle,
                    source_rectangle, GraphicsUnit.Pixel);
            }

            imageProcessing(area);
            label6.Show();
        }



        /// <summary>
        /// Кнопка распознавания
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            ImagesFunctions.getBinMatrix(inputShape, shape);

            int[] sums = { 0, 0, 0, 0 };
            string[] syms = { "Р", "М", "Е", "В" };

            sums[0] = MathFunctions.countSumOfWeight(inputShape, shapeP);
            sums[1] = MathFunctions.countSumOfWeight(inputShape, shapeM);
            sums[2] = MathFunctions.countSumOfWeight(inputShape, shapeE);
            sums[3] = MathFunctions.countSumOfWeight(inputShape, shapeB);

            int max = sums.Max();
            int index = Array.IndexOf(sums, max);

            MessageBox.Show("Распознана буква: " + syms[index]);
        }

        /// <summary>
        /// Обучить Р
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            ImagesFunctions.getBinMatrix(inputShape, shape);

            shapeP = MathFunctions.arraySummator(shapeP, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.correctTarget));
            shapeM = MathFunctions.arraySummator(shapeM, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeE = MathFunctions.arraySummator(shapeE, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeB = MathFunctions.arraySummator(shapeB, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));

            Logger.consoleLog(inputShape, shapeP, shapeM, shapeE, shapeB);

            label7.Show();

            label8.Text = (Int32.Parse(label8.Text) + 1).ToString();
        }

        /// <summary>
        /// Обучить М
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            ImagesFunctions.getBinMatrix(inputShape, shape);

            shapeP = MathFunctions.arraySummator(shapeP, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeM = MathFunctions.arraySummator(shapeM, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.correctTarget));
            shapeE = MathFunctions.arraySummator(shapeE, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeB = MathFunctions.arraySummator(shapeB, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));

            Logger.consoleLog(inputShape, shapeP, shapeM, shapeE, shapeB);

            label7.Show();

            label9.Text = (Int32.Parse(label9.Text) + 1).ToString();
        }

        /// <summary>
        /// Обучить Е
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            ImagesFunctions.getBinMatrix(inputShape, shape);

            shapeP = MathFunctions.arraySummator(shapeP, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeM = MathFunctions.arraySummator(shapeM, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeE = MathFunctions.arraySummator(shapeE, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.correctTarget));
            shapeB = MathFunctions.arraySummator(shapeB, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));

            Logger.consoleLog(inputShape, shapeP, shapeM, shapeE, shapeB);

            label7.Show();

            label10.Text = (Int32.Parse(label10.Text) + 1).ToString();
        }

        /// <summary>
        /// Обучить В
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            ImagesFunctions.getBinMatrix(inputShape, shape);

            shapeP = MathFunctions.arraySummator(shapeP, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeM = MathFunctions.arraySummator(shapeM, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeE = MathFunctions.arraySummator(shapeE, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.incorrectTarget));
            shapeB = MathFunctions.arraySummator(shapeB, MathFunctions.calculateWeights(inputShape, Properties.Settings.Default.correctTarget));

            Logger.consoleLog(inputShape, shapeP, shapeM, shapeE, shapeB);

            label7.Show();

            label11.Text = (Int32.Parse(label11.Text) + 1).ToString();
        }
    }
}
