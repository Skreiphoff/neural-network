﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NeuNet
{
    class FileSystem
    {
        /// <summary>
        /// Установка пути открытия файлов по умолчанию
        /// </summary>
        /// <returns></returns>
        protected internal static string choose_file()
        {
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Properties.Settings.Default.defaultImagePath;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }

                return filePath;
            }
        }
    }
}
