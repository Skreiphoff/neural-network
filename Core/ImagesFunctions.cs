﻿using System;
using System.Drawing;

namespace NeuNet.Core
{
    class ImagesFunctions
    {
        /// <summary>
        /// Изменение размера изображения
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        internal static Bitmap resizeBitmap(Bitmap bmp, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(bmp, 0, 0, width, height);
            }

            return result;
        }
        /// <summary>
        /// Перевод изображения в черно-белый формат
        /// </summary>
        /// <param name="bmp"></param>
        internal static Bitmap toGrayScale(Bitmap bmp)
        {
            int rgb;
            Color c;

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    c = bmp.GetPixel(x, y);
                    rgb = (int)Math.Round(.299 * c.R + .587 * c.G + .114 * c.B);
                    bmp.SetPixel(x, y, Color.FromArgb(rgb, rgb, rgb));
                }
            }

            return bmp;
        }
        /// <summary>
        /// Функция для обрезки пустых зон вокруг текста
        /// </summary>
        /// <param name="bmp"></param>
        internal static Bitmap Crop(Bitmap bmp)
        {
            int w = bmp.Width;
            int h = bmp.Height;

            Func<int, bool> allWhiteRow = row =>
            {
                for (int i = 0; i < w; ++i)
                    if (bmp.GetPixel(i, row).R != 255)
                        return false;
                return true;
            };

            Func<int, bool> allWhiteColumn = col =>
            {
                for (int i = 0; i < h; ++i)
                    if (bmp.GetPixel(col, i).R != 255)
                        return false;
                return true;
            };

            int topmost = 0;
            for (int row = 0; row < h; ++row)
            {
                if (allWhiteRow(row))
                    topmost = row;
                else break;
            }

            int bottommost = 0;
            for (int row = h - 1; row >= 0; --row)
            {
                if (allWhiteRow(row))
                    bottommost = row;
                else break;
            }

            int leftmost = 0, rightmost = 0;
            for (int col = 0; col < w; ++col)
            {
                if (allWhiteColumn(col))
                    leftmost = col;
                else
                    break;
            }

            for (int col = w - 1; col >= 0; --col)
            {
                if (allWhiteColumn(col))
                    rightmost = col;
                else
                    break;
            }

            if (rightmost == 0) rightmost = w; // As reached left
            if (bottommost == 0) bottommost = h; // As reached top.

            int croppedWidth = rightmost - leftmost;
            int croppedHeight = bottommost - topmost;

            if (croppedWidth == 0) // No border on left or right
            {
                leftmost = 0;
                croppedWidth = w;
            }

            if (croppedHeight == 0) // No border on top or bottom
            {
                topmost = 0;
                croppedHeight = h;
            }

            try
            {
                var target = new Bitmap(croppedWidth, croppedHeight);
                using (Graphics g = Graphics.FromImage(target))
                {
                    g.DrawImage(bmp,
                      new RectangleF(0, 0, croppedWidth, croppedHeight),
                      new RectangleF(leftmost, topmost, croppedWidth, croppedHeight),
                      GraphicsUnit.Pixel);
                }
                return target;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("Values are topmost={0} btm={1} left={2} right={3} croppedWidth={4} croppedHeight={5}", topmost, bottommost, leftmost, rightmost, croppedWidth, croppedHeight),
                  ex);
            }
        }
        /// <summary>
        /// Получение бинарной матрицы изображения
        /// </summary>
        internal static void getBinMatrix(int[] inputShape, Bitmap shape)
        {
            // Проход по чанкам
            for (int i = 0; i < Properties.Settings.Default.matrixSize; i++)
            {
                for (int j = 0; j < Properties.Settings.Default.matrixSize; j++)
                {
                    int chunkIdent = 0;
                    int chunkFill = 0;

                    // Проход по чанку
                    for (int x = 0 + (j * 10); x < (1 + j) * 10; x++)
                    {
                        for (int y = 0 + (i * 10); y < (1 + i) * 10; y++)
                        {
                            Color pixel = shape.GetPixel(x, y);

                            if (pixel.R < Properties.Settings.Default.ident && pixel.G < Properties.Settings.Default.ident && pixel.B < Properties.Settings.Default.ident)
                            {
                                chunkIdent++;
                            }
                        }
                    }

                    if (chunkIdent > 10)
                    {
                        chunkFill = 1;
                    }

                    inputShape[i * Properties.Settings.Default.matrixSize + j] = chunkFill;
                }
            }
        }
       
    }
}

