﻿using System;

namespace NeuNet.Core
{
    class Logger
    {
        /// <summary>
        /// Функция для логгирования
        /// </summary>
        /// <param name="inputShape"></param>
        /// <param name="shapeP"></param>
        /// <param name="shapeM"></param>
        /// <param name="shapeE"></param>
        /// <param name="shapeB"></param>
        internal static void consoleLog(int[] inputShape,int[] shapeP, int[] shapeM, int[] shapeE, int[] shapeB)
        {
            Console.WriteLine("Входной вектор:");
            logger(inputShape);

            Console.WriteLine("Р:");
            logger(shapeP);

            Console.WriteLine("М:");
            logger(shapeM);

            Console.WriteLine("Е:");
            logger(shapeE);

            Console.WriteLine("В:");
            logger(shapeB);

        }
        private static void logger(int[] model)
        {
            for (int i = 0; i < Properties.Settings.Default.matrixSize * Properties.Settings.Default.matrixSize; i++)
            {
                Console.Write(model[i] + " ");
                if ((i + 1) % Properties.Settings.Default.matrixSize == 0 && i > 0)
                {
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
        }

    }
}
