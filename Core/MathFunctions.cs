﻿using System.Linq;

namespace NeuNet
{
    class MathFunctions
    {   
        /// <summary>
        /// Функция для суммирования двух массивов
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public static int[] arraySummator(int[] a, int[] b)
        {
            return a.Zip(b, (x, y) => x + y).ToArray();
        }

        /// <summary>
        /// Функция для взвешивания весов
        /// </summary>
        /// <param name="model"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static int[] calculateWeights(int[] model, int target)
        {
            int[] result = new int[Properties.Settings.Default.matrixSize * Properties.Settings.Default.matrixSize];
            for (int i = 0; i < Properties.Settings.Default.matrixSize * Properties.Settings.Default.matrixSize; i++)
            {
                result[i] = model[i] * target;
            }
            return result;
        }

        /// <summary>
        /// Функция подсчета суммы весов
        /// </summary>
        /// <param name="model"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        public static int countSumOfWeight(int[] model, int[] weight)
        {
            int sum = 0;
            for (int i = 0; i < Properties.Settings.Default.matrixSize * Properties.Settings.Default.matrixSize; i++)
            {
                sum += model[i] * weight[i];
            }
            return sum;
        }

    }
}
